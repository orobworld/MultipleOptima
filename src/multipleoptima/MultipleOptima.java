package multipleoptima;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 * Demonstrates detection of multiple optima in an LP model.
 * @author Paul A. Rubin <rubin@msu.edu>
 */
public final class MultipleOptima {

  private static final double FUZZ = 1.0e-6;  // comparison fuzz limit
  private static final double INF = 1e20;     // test value for infinity
                                              // (unbounded in model terms)

  /**
   * Dummy constructor.
  */
  private MultipleOptima() { }

  /**
   * Main method.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    try {
      /*
       * Model 1:
       *   max x
       *   s.t. x <= 1, x + y <= 3, x >= 0, y >= 0
       */
      System.out.println("\n*** LP1");
      IloCplex lp = new IloCplex();
      IloNumVar[] vars = new IloNumVar[2];
      vars[0] = lp.numVar(0, 1, "x");
      vars[1] = lp.numVar(0, Double.MAX_VALUE, "y");
      double[] obj = new double[] {1, 0};
      lp.addMaximize(lp.scalProd(obj, vars));
      IloRange[] cons = new IloRange[1];
      cons[0] = lp.addLe(lp.sum(vars), 3, "C1");
      solve(lp, vars, obj, cons);
      /*
       * Model 2:
       *   max x + 2y
       *   s.t. x <= 1, x + y <= 3, x + 2y <= 0, x >= 0, y >= 0
       */
      System.out.println("\n*** LP2");
      lp = new IloCplex();
      vars[0] = lp.numVar(0, 1, "x");
      vars[1] = lp.numVar(0, Double.MAX_VALUE, "y");
      obj = new double[] {1, 2};
      lp.addMaximize(lp.scalProd(obj, vars));
      cons = new IloRange[2];
      cons[0] = lp.addLe(lp.sum(vars), 3, "C1");
      cons[1] = lp.addLe(lp.scalProd(obj, vars), 0, "C2");
      solve(lp, vars, obj, cons);
    } catch (IloException ex) {
      System.out.println("Something ugly happened: " + ex.getMessage());
      System.exit(1);
    }
  }

  /**
   * Solve the problem (or die trying), then display the solution and objective
   * and RHS sensitivity.
   * @param lp the LP problem
   * @param vars the variables in the problem
   * @param obj the objective coefficients of the variables
   * @param constraints the vector of constraints
   */
  private static void solve(final IloCplex lp, final IloNumVar[] vars,
                            final double[] obj, final IloRange[] constraints) {
    try {
      if (lp.solve() && lp.getStatus() == IloCplex.Status.Optimal) {
        System.out.println("Optimal value = " + lp.getObjValue());
        System.out.println("Optimal solution: x = " + lp.getValue(vars[0])
                           + ", y = " + lp.getValue(vars[1]));
        double[] lower = new double[vars.length];
        double[] upper = new double[vars.length];
        // Show the objective sensitivity.
        lp.getObjSA(lower, upper, vars);
        System.out.println("\nObjective ranging table:\n"
                           + "Variable            low           current"
                           + "            high");
        for (int i = 0; i < vars.length; i++) {
          System.out.println(vars[i].getName()
                             + "\t\t" + format(lower[i])
                             + "\t" + format(obj[i])
                             + "\t" + format(upper[i]));
        }
        // Check for possible multiple optima.
        for (int i = 0; i < vars.length; i++) {
          if (obj[i] - lower[i] < FUZZ) {
            System.out.println("\nPOSSIBLE multiple optima: "
                               + vars[i].getName()
                               + " has current coefficient " + obj[i]
                               + ", lower limit " + format(lower[i]));
          }
          if (upper[i] - obj[i] < FUZZ) {
            System.out.println("\nPOSSIBLE multiple optima: "
                               + vars[i].getName()
                               + " has current coefficient " + obj[i]
                               + ", upper limit " + format(upper[i]));
          }
        }
        // Check for degeneracy.
        double[] lblo = new double[vars.length];
        double[] lbhi = new double[vars.length];
        double[] ublo = new double[vars.length];
        double[] ubhi = new double[vars.length];
        lp.getBoundSA(lblo, lbhi, ublo, ubhi, vars);
        System.out.println("\nBounds ranging table:\n"
                           + "Variable           LB low             "
                           + "LB            LB high         UB low         "
                           + "UB            UB high");
        for (int i = 0; i < vars.length; i++) {
          System.out.println(vars[i].getName() + "\t\t"
                             + format(lblo[i]) + "\t"
                             + format(vars[i].getLB()) + "\t"
                             + format(lbhi[i]) + "\t"
                             + format(ublo[i]) + "\t"
                             + format(vars[i].getUB()) + "\t"
                             + format(ubhi[i]));
        }
        for (int i = 0; i < vars.length; i++) {
          double[] bds = new double[] {vars[i].getLB(), vars[i].getUB()};
          if ((bds[0] > -INF)
              && ((bds[0] - lblo[i] < FUZZ) || (lbhi[i] - bds[0] < FUZZ))) {
            System.out.println("\nDEGENERACY: lower bound of "
                               + vars[i].getName());
          }
          if ((bds[1] < INF)
               && ((bds[1] - ublo[i] < FUZZ) || (ubhi[i] - bds[1] < FUZZ))) {
            System.out.println("\nDEGENERACY: upper bound of "
                               + vars[i].getName());
          }
        }
        double[] rhslo = new double[constraints.length];
        double[] rhshi = new double[constraints.length];
        lp.getRHSSA(rhslo, rhshi, constraints);
        System.out.println("\nConstraint ranging table:\n"
                           + "Constraint            Low          "
                           + "RHSlo            RHShi          High");
        for (int i = 0; i < constraints.length; i++) {
          System.out.println(constraints[i].getName() + "\t\t"
                             + format(rhslo[i]) + "\t"
                             + format(constraints[i].getLB()) + "\t"
                             + format(constraints[i].getUB()) + "\t"
                             + format(rhshi[i]));
        }
        for (int i = 0; i < constraints.length; i++) {
          double[] bds =
            new double[] {constraints[i].getLB(), constraints[i].getUB()};
          if ((bds[0] > -INF)
              && ((bds[0] - rhslo[i] < FUZZ)) || (rhshi[i] - bds[0] < FUZZ)) {
            System.out.println("\nDEGENERACY: lower bound of "
                               + constraints[i].getName());
          }
          if ((bds[1] < INF)
              && ((rhshi[i] - bds[1] < FUZZ) || (bds[1] - rhslo[i] < FUZZ))) {
            System.out.println("\nDEGENERACY: upper bound of "
                               + constraints[i].getName());
          }
        }
      } else {
        System.out.println("Didn't find a solution? " + lp.getStatus());
        System.exit(2);
      }
    } catch (IloException ex) {
      System.out.println("Something ugly happened: " + ex.getMessage());
      System.exit(1);
    }
  }

  /**
   * Clean up doubles to catch CPLEX's various versions of "infinity".
   * @param x the value to clean up
   * @return a sanitized string representation of x
   */
  private static String format(final double x) {
    if (x <= -INF) {
      return "-Infinity";
    } else if (x >= INF) {
      return "+Infinity";
    } else {
      return String.format("%9.2f", x);
    }
  }
}
