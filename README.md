This is Java source code demonstrating sensitivity analysis and the detection of multiple optimal solutions to a linear program. The code relies on the CPLEX solver library.

A description of the problems solved and results appears in the 2012 blog post [Detecting Multiple Optima in an LP](https://orinanobworld.blogspot.com/2012/10/detecting-multiple-optima-in-lp.html).

The code is open source under a [Creative Commons 3.0 license](http://creativecommons.org/licenses/by/3.0/deed.en_US).